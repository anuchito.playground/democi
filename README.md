# Gitlab CI/CD Demo

This is a demo project to show how to use Gitlab CI/CD
by using the `Gitlab Runner` on a local machine.
using docker image `gitlab/gitlab-runner:latest`

## Prerequisites
1. `Disable shared runners` in `Settings` -> `CI/CD` -> `Runners`

## How to use

1. Craete new project on Gitlab
2. create file `.gitlab-ci.yml` in the root of the project
3. Add the following content to the file

```yaml
job-run-test:
  script:
    - echo "hello demo job run"
```

4. Commit and push the changes
5. Go to `CI/CD` -> `Pipelines` to see the pipeline running
6. Go to `CI/CD` -> `Jobs` to see the job stuck in `pending` state

## How to fix

1. Go to `Settings` -> `CI/CD` -> `Runners`
2. `New project runner` -> `Set up a specific Runner manually`
	2.1. checkbox `Run untagged jobs`
3. Click `Create runner`
4. Copy the `Registration token` (you will need it later on GITLAB_REGISTRATION_TOKEN)
5. start the `Gitlab Runner` container by using docker-compose

```bash
GITLAB_URL=https://gitlab.com GITLAB_REGISTRATION_TOKEN=${GITLAB_REGISTRATION_TOKEN} docker-compose up
```

6. watch the logs of the `Gitlab Runner` container

```bash
docker stats
```

# NOTE: Register the runner step by step
```bash
docker run -d --name gitlab-runner --restart always \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-v ${PWD}/config:/etc/gitlab-runner \
	gitlab/gitlab-runner:latest
```

```bash
docker exec -it gitlab-runner bash

cat /etc/gitlab-runner/

# Register the runner
gitlab-runner register \
        --non-interactive \
        --name "my gitlab runner" \
        --url "https://gitlab.com" \
        --registration-token "${GITLAB_REGISTRATION_TOKEN}" \
        --clone-url "https://gitlab.com" \
        --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
        --locked="false" \
        --executor="docker" \
        --docker-privileged="true" \
        --paused="true" \
        --docker-cpus="1" \
        --docker-image="docker:stable" \
        --run-untagged="true"
```

5. Go to `CI/CD` -> `Pipelines` to see the pipeline running
